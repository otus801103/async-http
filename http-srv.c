#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

#define MAX_EVENTS 64
#define BUFFER_LENGTH 1024

int make_socket_non_blocking(int socket_fd) {
    int current_flags = fcntl(socket_fd, F_GETFL, 0);
    return current_flags == -1 ? -1 : fcntl(socket_fd, F_SETFL, current_flags | O_NONBLOCK);
}

void send_client_file(int client_socket, int file_descriptor) {
    char file_buffer[BUFFER_LENGTH];
    ssize_t read_bytes;

    while ((read_bytes = read(file_descriptor, file_buffer, BUFFER_LENGTH)) > 0) {
        if (send(client_socket, file_buffer, read_bytes, 0) == -1) {
            perror("Error sending file content");
            break;
        }
    }
}

void serve_http_response(int socket_fd, const char *status_code, const char *content_type, const char *response_body, size_t response_size) {
    char http_header[BUFFER_LENGTH];

    response_body = response_body != NULL ? response_body : "";
    content_type = content_type != NULL ? content_type : "text/html; charset=utf-8";
    // Здесь изменяем сравнение, чтобы работать с беззнаковым типом
    response_size = (response_body != NULL && response_size == (size_t)-1) ? strlen(response_body) : response_size;

    snprintf(http_header, sizeof(http_header),
             "HTTP/1.1 %s\r\n"
             "Content-Type: %s\r\n"
             "Content-Length: %zu\r\n" // Используем спецификатор %zu для size_t
             "\r\n",
             status_code, content_type, response_size);

    if (send(socket_fd, http_header, strlen(http_header), 0) == -1) {
        perror("Error sending HTTP header");
        return;
    }

    if (response_size > 0 && send(socket_fd, response_body, response_size, 0) == -1) {
        perror("Error sending HTTP body");
    }
}

void process_client_request(int client_socket, const char *root_directory) {
    char request_buffer[BUFFER_LENGTH];
    ssize_t bytes_received = recv(client_socket, request_buffer, sizeof(request_buffer), 0);

    if (bytes_received <= 0) {
        if (bytes_received < 0) perror("Error receiving data");
    } else {
        request_buffer[bytes_received] = '\0';
        if (strncmp(request_buffer, "GET ", 4) == 0) {
            char *requested_path = request_buffer + 4;
            char *path_end = strchr(requested_path, ' ');

            if (path_end) {
                *path_end = '\0';
                char full_path[BUFFER_LENGTH];
                snprintf(full_path, sizeof(full_path), "%s%s", root_directory, requested_path);

                int file_fd = open(full_path, O_RDONLY);
                if (file_fd == -1) {
                    const char *error_message = "The requested resource could not be found on this server.";
                    const char *status = errno == EACCES ? "403 Forbidden" : "404 Not Found";
                    serve_http_response(client_socket, status, "text/plain", error_message, -1);
                } else {
                    serve_http_response(client_socket, "200 OK", "text/plain", NULL, 0);
                    send_client_file(client_socket, file_fd);
                    close(file_fd);
                }
            }
        } else {
            serve_http_response(client_socket, "400 Bad Request", "text/plain", "Invalid request syntax.", -1);
        }
    }
    close(client_socket);
}

int main(int argc, char *argv[]) {
    if (argc < 4) {
        fprintf(stderr, "Usage: %s <directory> <address> <port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const char *root_directory = argv[1];
    const char *bind_address = argv[2];
    int port_number = atoi(argv[3]);

    int server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == -1) {
        perror("Error creating socket");
        exit(EXIT_FAILURE);
    }

    make_socket_non_blocking(server_socket);

    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(bind_address);
    server_addr.sin_port = htons(port_number);

    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error binding socket");
        exit(EXIT_FAILURE);
    }

    if (listen(server_socket, SOMAXCONN) == -1) {
        perror("Error listening on socket");
        exit(EXIT_FAILURE);
    }

    int epoll_fd = epoll_create1(0);
    if (epoll_fd == -1) {
        perror("Error creating epoll instance");
        exit(EXIT_FAILURE);
    }

    struct epoll_event main_event;
    main_event.data.fd = server_socket;
    main_event.events = EPOLLIN | EPOLLET;
    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, server_socket, &main_event) == -1) {
        perror("Error adding socket to epoll");
        exit(EXIT_FAILURE);
    }

    struct epoll_event *events_buffer = calloc(MAX_EVENTS, sizeof(main_event));
    if (!events_buffer) {
        perror("Error allocating memory");
        exit(EXIT_FAILURE);
    }

    while (1) {
        int event_count = epoll_wait(epoll_fd, events_buffer, MAX_EVENTS, -1);
        for (int i = 0; i < event_count; i++) {
            if (events_buffer[i].data.fd == server_socket) {
                while (1) {
                    struct sockaddr client_addr;
                    socklen_t client_addr_len = sizeof(client_addr);
                    int client_socket = accept(server_socket, &client_addr, &client_addr_len);
                    if (client_socket == -1) {
                        if (errno == EAGAIN || errno == EWOULDBLOCK) {
                            break; // No more incoming connections
                        } else {
                            perror("Error accepting connection");
                            break;
                        }
                    }

                    make_socket_non_blocking(client_socket);
                    struct epoll_event client_event;
                    client_event.data.fd = client_socket;
                    client_event.events = EPOLLIN | EPOLLET;
                    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, client_socket, &client_event) == -1) {
                        perror("Error adding client socket to epoll");
                        close(client_socket);
                    }
                }
            } else {
                process_client_request(events_buffer[i].data.fd, root_directory);
            }
        }
    }

    close(epoll_fd);
    free(events_buffer);
    close(server_socket);

    return EXIT_SUCCESS;
}

